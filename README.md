**Blogger optimizer**

Test task for Yoloco. Implemented by Mikhail Tkachenko (https://t.me/man12102)

**Task**

Task is to implement an algorithm which finds most optimal subset of bloggers. Optimal means that intersection of blogger subscribers is minimal.

**Algorithm details**

Algorithm tends to minimize sum of intersecting parts between bloggers 

![Image of diagram]
(img/diagram.png)

Given input, we know intersection of each pair of bloggers as (total_subscribers - unique_subscribers). Sum of intersections grows up as we add a new blogger to the pool of optimal bloggers. 
This means that using greedy approach, optimal candidate to pool of bloggers has to have minimal sum of intersections with each pool member. 
This is the idea I use in this task. Algorithm iteratively adds optimal candidate to the pool of bloggers using greedy approach.