class BloggerOptimizer:
    def __init__(self, filename=''):
        """
        Init
        :param data: List of tuples (unique, total): [(),(),...,()]
        """
        self.nodes = []  # names of nodes (bloggers)
        self.graph = []  # Adjacency matrix
        self.create_graph(filename)

    def create_graph(self, filename):
        """
        Creates an adjacency matrix for graph interpretation
        using data provided
        :param filename: .csv file with matrix of bloggers (to be implemented)
        :return: graph in adjacency matrix, names of nodes (bloggers)
        """
        if not filename:
            #
            # self.graph = [
            #     [(0, 0, None), (134, 645, 134/645), (500, 700, 500/700),
            #      (400, 550, 400/550)],
            #     [(134, 645, 134/645), (0, 0, None), (452, 900, 452/900),
            #      (452, 564, 452/564)],
            #     [(500, 700, 500/700), (452, 900, 452/900), (0, 0, None),
            #      (543, 642, 543/642)],
            #     [(400, 550, 400/550), (452, 564, 452/564), (543, 642, 543/642),
            #      (0, 0, None)],
            # ]
            # Third number - percentage of intersection
            self.graph = [[(0, 0, None), (100, 200, 0.5), (250, 500, 0.5)],
                          [(100, 200, 0.5), (0, 0, None), (50, 700, 1 - 0.07)],
                          [(250, 500, 0.5), (50, 700, 1 - 0.07), (0, 0, None)]]
            self.nodes = [i for i in [0, 1, 2]]
        else:
            pass  # TODO: parse .csv file

    def __find_optimal__(self, pool):
        """
        Finds optimal candidate to pool from graph using greedy approach
        :param pool: pool of optimal bloggers
        :return:
        """
        if len(pool) == 0:
            potential_pool = []
            optimal = None
            for i in range(len(self.graph[0])):
                for j in range(len(self.graph)):
                    pair = self.graph[i][j]
                    if pair[2] is not None:
                        if not optimal or (pair[2] <= optimal[2] and (pair[1] > optimal[1])):
                            optimal = pair
                            potential_pool = [self.nodes[i], self.nodes[j]]
            return potential_pool

        winner = None
        percentage_sum_score = 1000000000
        total_subscribers_score = 0
        for node in filter(lambda x: x not in pool, self.nodes):
            percentage_sum = 1000000000
            total_subscribers = 0
            for poolnode in pool:
                percentage_sum += self.graph[node][poolnode][2]
                total_subscribers += self.graph[node][poolnode][0]
            if node or (percentage_sum <= percentage_sum_score and total_subscribers > total_subscribers_score):
                winner = node
                percentage_sum_score = percentage_sum
                total_subscribers_score = total_subscribers
        return [winner]

    def define_pool(self, size):
        """
        Defines pool of bloggers to be picked
        :param size: number of bloggers to be picked
        :return: list of bloggers to be picked
        """
        if size > len(self.nodes):
            raise ValueError('Subset size must be less than set size')

        pool = []  # pool to be returned
        for i in range(size - 1):  # adds 2 in the first iteration
            pool_candidate = self.__find_optimal__(pool)
            pool.extend(pool_candidate)
        return pool
